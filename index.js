const express = require('express')
const bodyParser = require("body-parser");
const app = express()
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.get("/", (req, res) => {
    res.json({ message: "Welcome to node js application." });
  });
  require("./app/routes/customer.routes.js")(app);
app.get('/ping', (req,res) => res.send('Pong!!'))


module.exports = app


// parse requests of content-type - application/x-www-form-urlencoded


// simple route



