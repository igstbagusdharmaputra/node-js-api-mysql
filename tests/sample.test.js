// describe('Sample Test', () => {
//     it('should test that true === true', () => {
//       expect(true).toBe(true);
//     });
//   });

const app = require('../index') // Link to your server file
const supertest = require('supertest')
const request = supertest(app)

it('Call the /ping endpoint', async done => {
    const res = await request.get('/ping')
    expect(res.status).toBe(200)
    expect(res.text).toBe('Pong!!')
    done()
})